package org.jax.mgi;

public class Main {

	public static void main(String[] args) {
		System.out.println(sanitizeInput("search terms"));
		System.out.println(sanitizeInput("diabetes"));
		System.out.println(sanitizeInput("diabetes, MP:0004031, "));
		System.out.println(sanitizeInput("diabetes, AND MP:0004031, "));
		System.out.println(sanitizeInput("diabetes AND insulitis "));
		System.out.println(sanitizeInput("alzheimer"));
		System.out.println(sanitizeInput("alzheimer disease"));
		System.out.println(sanitizeInput("alzheimer learning"));
		System.out.println(sanitizeInput("alzheimer disease learning "));
		System.out.println(sanitizeInput("alzheimer AND learning"));
		System.out.println(sanitizeInput("alzheimer, schizophrenia"));
		System.out.println(sanitizeInput("alzheimer, AND schizophrenia"));
		System.out.println(sanitizeInput("alzheimer AND schizophrenia"));
		System.out.println(sanitizeInput("alzheimer disease AND schizophrenia"));
		System.out.println(sanitizeInput("alzheimer AND NOT schizophrenia"));
		System.out.println(sanitizeInput("104300 (OMIM ID for Alzheimer Disease, AD)"));
		
		System.out.println(sanitizeInput("104300, 114480 (OMIM ID for Breast Cancer)"));
		System.out.println(sanitizeInput("104300 AND 114480"));
		System.out.println(sanitizeInput("MP:0006325"));
		System.out.println(sanitizeInput("MP:0001265 MP:0006325"));
		System.out.println(sanitizeInput("MP:0001265 AND MP:0006325"));
		System.out.println(sanitizeInput("\"hearing loss\" AND MP:0006325"));
		System.out.println(sanitizeInput("\"impaired hearing\" AND MP:0006325"));
		System.out.println(sanitizeInput("dwarf AND \"impaired hearing\" AND kyphosis"));
		System.out.println(sanitizeInput("MP:0001265 AND MP:0006325 AND kyphosis"));
		System.out.println(sanitizeInput("MP:0006325 AND NOT MP:0001394"));
		System.out.println(sanitizeInput("parkinson* AND (tremor OR dystonia)"));
		
	}
	
	private static String sanitizeInput(String s) {
		s = QueryParser.removeUnmatched(s,'"');
		s = QueryParser.removeUnmatched(s,'(',')');
		s = s.replace("\"\""," ");
		s = s.replace("()"," ");
		return s;
	}
}
